0. CONFIGURATION

git config --global user.name "Taras Mandzak"
git config --global user.email "taras.x.mandzak@ericsson.com"
git config --global -l

1. INITIAL STEPS

git status
git init

<https://www.gitignore.io/>

git add edit.txt
git add --all
git status
git commit -m "Initial commit"
git log
git status

2. CHANGES

<edit, delete, create files>

git status
git add --all
git status
git commit -m "Edit;Delete;Create"
git status
git log

<rename, delete exe>

git status
git add --all
git commit -m "Rename; Delete EXE"
git log --oneline
gitk

<any chenges>

git reset --hard
git branch dev
git checkout dev

3. BRANCHES AND MERGING

<Edit edit.txt with letters>
git add --all
git commit -m "Edit edit.txt with letters"

<Delete new.txt>
git add --all
git commit -m "Delete new.txt"


git checkout master
<Edit edit.txt with numbers>
git add --all
git commit -m "Edit edit.txt with numbers"


git checkout dev
git merge master
git status
<edit edit.txt>
git add --all
git commit -m "Merge commit"

git checkout master
git merge dev

git branch -D dev

4. REVERT CHANGES

git rm -r .
gitk
git checkout <ID from gitk> .
git add --all
git commit -m "Back to Initial Commit"









